<?php

abstract class Forma
{
    public $tipoDeForma = 'Forma Abstrata';

    public function imprimeForma()
    {
        echo $this ->tipoDeForma . ' com Àrea de: ' . $this->calculaArea();
    }

    abstract public function calculaArea();
}

class Quadrado extends Forma
{
    public $lado;
    public function __construct (float $varLado)

    {
        $this-> tipoDeForma = 'Quadrado';
        $this-> lado = $varLado;
    }
    public function calculaArea()
    {
        return $this-> lado * $this-> lado;
    }
}

$obj = new Quadrado (5);
$obj -> imprimeForma ();

$obj = new Quadrado (100);
$obj -> imprimeForma ();