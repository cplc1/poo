<?php

abstract class Conta
{
    private $tipoDaConta;
    public function  getTipoDaConta(){
        return $this-> tipoDaConta;
    }
    public function setTipoDaConta(string $tipoDaconta){
        $this-> tipoDaConta = $tipoDaconta;
    }

    public $agencia;
    public $conta;
    protected $saldo;


    public function deposito(float $valor)
    {
        if ($valor >  0) {
            //$this-> saldo = $this->saldo - $valor;
            $this->saldo += $valor;
            echo "Deposito realizado com sucesso <br>";
        } else 
        {
            echo "Valor Deposito invalido!!! <br>";
        }
    }

    public function saque(float $valor)
    {
        //$this-> saldo = $this->saldo - $valor;
        if ($this->saldo = $this->saldo - $valor) {
            $this->saldo += $valor;
            echo "Saque realizado com sucesso <br>";
        } else {
            echo "Saldo  insuficiente!!! <br>";
        }
    }

    abstract public function calculaSaldo();

    public function imprimeExtrato()
    {
        echo "Conta " . $this->tipoDaConta .  " Agéncia: " . $this->agencia . "Conta" . $this->conta . "Saldo: " . $this->calculaSaldo();
    }
}

class Poupanca extends Conta
{
    public $reajuste;

    public function __construct(string $agencia, string $conta, float $reajuste)
    {
        $this->tipoDaConta = ('Poupança');
        $this->agencia = $agencia;
        $this->conta = $conta;
        $this->reajuste = $reajuste;
    }

    public function calculaSaldo()
    {
        return $this->saldo + ($this->saldo * $this->reajuste / 100);
    }
}

class Especial extends Conta
{
    public $saldoEspecial;
    public function __construct(string $agencia, string $conta, float $saldoEspecial)
    {
        $this->tipoDaConta = 'Especial';
        $this->agencia = $agencia;
        $this->conta = $conta;
        $this->saldoEspecial = $saldoEspecial;
    }

    public function calculaSaldo()
    {
        return $this->saldo + $this->saldoEspecial;
    }
}


$ctaPoupanca = new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoupanca-> saldo = -1500;
//Não pode acessar atributoprotegido
$ctaPoupanca->deposito(-1500);
$ctaPoupanca->saque(3000);
$ctaPoupanca->imprimeExtrato();

echo '<br>';

$ctaEspecial = new Especial('0055-2', '75588-42', 2300);
$ctaEspecial->deposito(1500);
$ctaEspecial->imprimeExtrato();
